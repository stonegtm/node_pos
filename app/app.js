const express = require('express')
const axios = require('axios')

//express app

const app = express();

//register view engine
app.set('view engine', 'ejs')

//listen for request
app.listen(8000)

app.get('/', async (req,res) =>{
   const test_api = await axios.get('https://api.nasa.gov/planetary/apod?api_key=UWwjIrksawd1a4owYUxVLTYr1qa1W0h4a6kVLdXu')
    // res.send('<h1>Stone</h1>')
    // res.sendFile('./views/index.html',{root:__dirname})
    res.render('index',{txxx: test_api.data})
})

app.get('/about',(req,res)=>{
    // res.sendFile('./views/about.html',{root:__dirname})
    // res.send('<h1>Stoneabout</h1>')
    res.render('about')

})


//redirect
app.get('/about-us',(req,res)=>{
    res.redirect('/about')
})

//404 page
app.use((req,res)=>{
    res.status(404).render('404')
})